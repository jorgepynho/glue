<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Glue</title>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/phaser/2.6.2/phaser.min.js"></script>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body id="eBody">
	<div id="glue-game"></div>
	<script type="text/javascript" src="glue.js"></script>
</body>
</html>
